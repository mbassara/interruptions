#!/bin/bash

cd ./src/modules/eth
make clean
make
sudo mknod /dev/eth c 321 0
sudo rmmod eth
sudo insmod eth.ko

cd ../kbrd
make clean
make
sudo mknod /dev/kbrd c 322 0
sudo rmmod kbrd
sudo insmod kbrd.ko

cd ../mouse
make clean
make
sudo mknod /dev/mouse c 323 0
sudo rmmod mouse
sudo insmod mouse.ko

cd ../disk
make clean
make
sudo mknod /dev/disk_ps c 324 0
sudo rmmod disk
sudo insmod disk.ko
