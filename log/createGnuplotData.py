import numpy
import sys




filesAndDeviceNames = sys.argv[1:]
pairsLeft = len(filesAndDeviceNames)


while pairsLeft > 0:
	fileName = sys.argv[-pairsLeft]
	deviceName = sys.argv[-pairsLeft+1]
	dataFile = open(fileName, 'r')
	numberOfLines = 0
	dataArray = []

	for data in dataFile.readlines():
		if data[0] is 'T' or data[1] is 'B':
			break
		dataArray.append(float(data))

	mean = numpy.mean(dataArray)
	standardDeviation = numpy.std(dataArray)

	print deviceName + ' ' + str(mean) + ' ' + str(standardDeviation)	
	pairsLeft -=2

