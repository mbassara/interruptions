#python createGnuplotData.py ./AD/disk1.log disk ./AD/eth1.log eth ./AD/mouse1.log mouse ./AD/kbrd1.log keyboard > AD1device.plot   
#python createGnuplotData.py ./AD/mouse_w_disk.log mouse  ./AD/disk_w_mouse.log disk ./AD/mouse_w_eth.log mouse ./AD/eth_w_mouse.log eth ./AD/mouse_w_keyboard.log mouse ./AD/kbrd_w_mouse.log keyboard \
#  ./AD/mouse_w_disk.log mouse
python createGnuplotData.py ./AD/mouse1.log mouse ./AD/mouse_w_disk.log mouseWithDisk ./AD/mouse_w_eth.log mouseWithEth ./AD/mouse_w_keyboard.log mouseWithKeyboard \
	 ./AD/mouse_w_ethDisk.log mouseWithEth\&Disk ./AD/mouse_w_kbrdDisk.log mouseWithKeyboard\&Disk ./AD/mouse_w_kbrdEth.log mouseWithKeyboard\&Ethernet \
	 ./AD/mouse4.log allInterrupts > mouse.plot

python createGnuplotData.py ./AD/kbrd1.log keyboard ./AD/kbrd_w_disk.log keyboardWithDisk ./AD/kbrd_w_eth.log keyboardWithEth ./AD/kbrd_w_mouse.log keyboardWithMouse \
	 ./AD/kbrd_w_ethDisk.log keyboardWithEth\&Disk ./AD/kbrd_w_mouseDisk.log keyboardWithMouse\&Disk ./AD/kbrd_w_mouseEth.log keyboardWithMouse\&Ethernet \
	 ./AD/kbrd4.log allInterrupts > keyboard.plot

python createGnuplotData.py ./AD/eth1.log eth ./AD/eth_w_disk.log ethWithDisk ./AD/eth_w_kbrd.log ethWithKeyboard ./AD/eth_w_mouse.log ethWithMouse \
	 ./AD/eth_w_kbrdDisk.log ethWithKeyboard\&Disk ./AD/eth_w_mouseDisk.log ethWithMouse\&Disk ./AD/eth_w_kbrdMouse.log ethWithMouse\&Keyboard \
	 ./AD/eth4.log allInterrupts > eth.plot

python createGnuplotData.py ./AD/disk1.log eth ./AD/disk_w_eth.log diskWithEth ./AD/disk_w_kbrd.log diskWithKeyboard ./AD/disk_w_mouse.log diskWithMouse \
	 ./AD/disk_w_ethKbrd.log diskWithKeyboard\&Eth ./AD/disk_w_ethMouse.log diskWithMouse\&Eth ./AD/disk_w_kbrdMouse.log diskWithMouse\&Keyboard \
	 ./AD/disk4.log allInterrupts > disk.plot	 


python createGnuplotData.py ./Maciek/mouse1.log mouse ./Maciek/mouse_w_disk.log mouseWithDisk ./Maciek/mouse_w_eth.log mouseWithEth ./Maciek/mouse_w_kbrd.log mouseWithKeyboard ./Maciek/mouse_w_disk_eth.log mouseWithEth\&Disk ./Maciek/mouse_w_disk_kbrd.log mouseWithKeyboard\&Disk ./Maciek/mouse_w_eth_kbrd.log mouseWithKeyboard\&Ethernet ./Maciek/mouse_w_disk_eth_kbrd.log mouseWithAll > ./Maciek/plot/mouse.dat

python createGnuplotData.py ./Maciek/disk1.log disk ./Maciek/disk_w_eth.log diskWithEthernet ./Maciek/disk_w_kbrd.log diskWithKeyboard ./Maciek/disk_w_mouse.log diskWithMouse ./Maciek/disk_w_eth_mouse.log diskWithEthernet\&Mouse ./Maciek/disk_w_kbrd_mouse.log diskWithKeyboard\&Mouse ./Maciek/disk_w_eth_kbrd.log diskWithEthernet\&Keyboard ./Maciek/disk_w_eth_kbrd_mouse.log diskWithAll > ./Maciek/plot/disk.dat

python createGnuplotData.py ./Maciek/kbrd1.log keyboard ./Maciek/kbrd_w_eth.log keyboardWithEthernet ./Maciek/kbrd_w_disk.log keyboardWithDisk ./Maciek/kbrd_w_mouse.log keyboardWithMouse ./Maciek/kbrd_w_eth_mouse.log keyboardWithEthernet\&Mouse ./Maciek/kbrd_w_disk_mouse.log keyboardWithDisk\&Mouse ./Maciek/kbrd_w_disk_eth.log keyboardWithDisk\&Ethernet ./Maciek/kbrd_w_disk_eth_mouse.log keyboardWithAll > ./Maciek/plot/kbrd.dat

python createGnuplotData.py ./Maciek/eth1.log ethernet ./Maciek/eth_w_kbrd.log ethernetWithKeyboard ./Maciek/eth_w_disk.log ethernetWithDisk ./Maciek/eth_w_mouse.log ethernetWithMouse ./Maciek/eth_w_kbrd_mouse.log ethernetWithKeyboard\&Mouse ./Maciek/eth_w_disk_mouse.log ethernetWithDisk\&Mouse ./Maciek/eth_w_disk_kbrd.log ethernetWithDisk\&Keyboard ./Maciek/eth_w_disk_kbrd_mouse.log ethernetWithAll > ./Maciek/plot/eth.dat
