#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");

static int ethernet_module_init(void);
static void ethernet_module_exit(void);
static void ethernet_tasklet(unsigned long times_tab_ptr);
irqreturn_t irq_ethernet_handler(int irq, void * data);
static int error(const char* info, int errno);

int ethernet_open(struct inode *inode, struct file *filp);
int ethernet_release(struct inode *inode, struct file *filp);
ssize_t ethernet_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos);;

struct file_operations ethernet_fops = {
    read: ethernet_read,
    open: ethernet_open,
    release: ethernet_release
};

module_init(ethernet_module_init);
module_exit(ethernet_module_exit);
