#include "eth.h"

#define ETH_DEV_MAJOR 321
#define ETH_IRQ_LINE 19
#define BUFF_LEN 128
#define TIMES_TAB_LEN 4096
#define TASKLET_POOL_LEN 60

int var;
struct tasklet_struct tasklet[TASKLET_POOL_LEN];
int irq_handler_count;
int tasklet_count;
char* buff;
struct timespec times_tab[TIMES_TAB_LEN];

/***************************
 *          INIT           *
 ***************************/

static int ethernet_module_init(void)
{
    int err;
    int i;

    for(i=0; i< TASKLET_POOL_LEN; ++i ){
        DECLARE_TASKLET(taskletToSchedule, ethernet_tasklet, (unsigned long) &times_tab);
        tasklet[i] = taskletToSchedule;

    }

    /* register /dev/eth */
    if ((err = register_chrdev(ETH_DEV_MAJOR, "eth", &ethernet_fops)) < 0) {
        return error("Cannot register the /dev/eth device", err);
    }

    irq_handler_count = 0;
    tasklet_count = 0;
    buff = (char*) kmalloc( BUFF_LEN, GFP_KERNEL);

    if (buff) {
        memset((void*) buff, 0,  BUFF_LEN);
        printk(KERN_INFO "Ethernet module - init\n");
        if ((err = request_irq(ETH_IRQ_LINE, irq_ethernet_handler, IRQF_SHARED, "eth", &var)) < 0) {
            return error("Cannot register the interruption service routine.", err);
        }
    }
    else {
        return error("Cannot allocate buffer.", -ENOMEM);
    }

    return 0;
}

/***************************
 *          EXIT           *
 ***************************/

static void ethernet_module_exit(void)
{
    printk(KERN_INFO "Ethernet module - exit\n");

    unregister_chrdev(ETH_DEV_MAJOR, "eth");

    free_irq(ETH_IRQ_LINE, &var);
}

/***************************
 *       IRQ HANDLER       *
 ***************************/

irqreturn_t irq_ethernet_handler(int irq, void * data)
{
    getnstimeofday(times_tab + (irq_handler_count % TIMES_TAB_LEN));
    irq_handler_count++;
    tasklet_schedule(tasklet +(irq_handler_count % TASKLET_POOL_LEN));

    return IRQ_NONE;
}

/***************************
 *         TASKLET         *
 ***************************/

static void ethernet_tasklet(unsigned long times_tab_ptr)
{
    struct timespec tasklet_time; 
    struct timespec handler_time = times_tab[tasklet_count % TIMES_TAB_LEN];
    getnstimeofday(&tasklet_time);

    tasklet_time.tv_sec -= handler_time.tv_sec;
    tasklet_time.tv_nsec -= handler_time.tv_nsec;

    times_tab[tasklet_count % TIMES_TAB_LEN] = tasklet_time;
    tasklet_count++;
}

/***************************
 *          OPEN           *
 ***************************/

int ethernet_open(struct inode *inode, struct file *filp)
{
    return 0;
}

/***************************
 *         RELEASE         *
 ***************************/

int ethernet_release(struct inode *inode, struct file *filp)
{
    return 0;
}

/***************************
 *          READ           *
 ***************************/

int read_times = 0;
ssize_t ethernet_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos)
{
    int len;
    memset(buff, 0, BUFF_LEN);

    if(read_times < TIMES_TAB_LEN) {
        snprintf(buff, BUFF_LEN, "%ld.%ld\n", times_tab[read_times].tv_sec, times_tab[read_times].tv_nsec / 1000);
    } else if(read_times == TIMES_TAB_LEN) {
        snprintf(buff, BUFF_LEN, "Top-half executions: %d\n", irq_handler_count);
    } else if(read_times == TIMES_TAB_LEN + 1) {
        snprintf(buff, BUFF_LEN, "Bottom-half executions: %d\n", tasklet_count);
    } else {
        read_times = 0;
        return 0;
    }

    read_times++;

    len = strlen(buff);
    copy_to_user(user_buf, buff, len);
    return len;
}

static int error(const char* info, int errno)
{
    printk(KERN_ALERT "%s Error: %d\n", info, errno);
    ethernet_module_exit();
    return errno;
}
