#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");

static int mouse_module_init(void);
static void mouse_module_exit(void);
static void mouse_tasklet(unsigned long times_tab_ptr);
irqreturn_t irq_mouse_handler(int irq, void * data);
static int error(const char* info, int errno);

int mouse_open(struct inode *inode, struct file *filp);
int mouse_release(struct inode *inode, struct file *filp);
ssize_t mouse_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos);;

struct file_operations mouse_fops = {
    read: mouse_read,
    open: mouse_open,
    release: mouse_release
};

module_init(mouse_module_init);
module_exit(mouse_module_exit);
