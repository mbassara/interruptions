#include "disk.h"

#define DISK_DEV_MAJOR 324
#define DISK_IRQ_LINE 21
#define BUFF_LEN 128
#define TIMES_TAB_LEN 4096
#define TASKLET_POOL_LEN 60

int var;
struct tasklet_struct tasklet[TASKLET_POOL_LEN];
int irq_handler_count;
int tasklet_count;
char* buff;
struct timespec times_tab[TIMES_TAB_LEN];



/***************************
 *          INIT           *
 ***************************/

static int disk_module_init(void)
{
    int i;
    int err;


    for(i=0; i< TASKLET_POOL_LEN; ++i ){
        DECLARE_TASKLET(taskletToSchedule, disk_tasklet, (unsigned long) &times_tab);
        tasklet[i] = taskletToSchedule;

    }


    /* register /dev/disk_ps */
    if ((err = register_chrdev(DISK_DEV_MAJOR, "disk_ps", &disk_fops)) < 0) {
        return error("Cannot register the /dev/disk_ps device", err);
    }

    irq_handler_count = 0;
    tasklet_count = 0;
    buff = (char*) kmalloc( BUFF_LEN, GFP_KERNEL);

    if (buff) {
        memset((void*) buff, 0,  BUFF_LEN);
        printk(KERN_INFO "Disk module - init\n");
        if ((err = request_irq(DISK_IRQ_LINE, irq_disk_handler, IRQF_SHARED, "disk", &var)) < 0) {
            return error("Cannot register the interruption service routine.", err);
        }
    }
    else {
        return error("Cannot allocate buffer.", -ENOMEM);
    }

    return 0;
}

/***************************
 *          EXIT           *
 ***************************/

static void disk_module_exit(void)
{
    printk(KERN_INFO "Disk module - exit\n");

    unregister_chrdev(DISK_DEV_MAJOR, "disk_ps");

    free_irq(DISK_IRQ_LINE, &var);
}

/***************************
 *       IRQ HANDLER       *
 ***************************/

irqreturn_t irq_disk_handler(int irq, void * data)
{
    getnstimeofday(times_tab + (irq_handler_count % TIMES_TAB_LEN));
    irq_handler_count++;
    tasklet_schedule(tasklet +(irq_handler_count % TASKLET_POOL_LEN));

    return IRQ_NONE;
}

/***************************
 *         TASKLET         *
 ***************************/

static void disk_tasklet(unsigned long times_tab_ptr)
{
    struct timespec tasklet_time; 
    struct timespec handler_time = times_tab[tasklet_count % TIMES_TAB_LEN];
    getnstimeofday(&tasklet_time);

    tasklet_time.tv_sec -= handler_time.tv_sec;
    tasklet_time.tv_nsec -= handler_time.tv_nsec;

    times_tab[tasklet_count % TIMES_TAB_LEN] = tasklet_time;
    tasklet_count++;
}

/***************************
 *          OPEN           *
 ***************************/

int disk_open(struct inode *inode, struct file *filp)
{
    return 0;
}

/***************************
 *         RELEASE         *
 ***************************/

int disk_release(struct inode *inode, struct file *filp)
{
    return 0;
}

/***************************
 *          READ           *
 ***************************/

int read_times = 0;
ssize_t disk_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos)
{
    int len;
    memset(buff, 0, BUFF_LEN);

    if(read_times < TIMES_TAB_LEN) {
        snprintf(buff, BUFF_LEN, "%ld.%ld\n", times_tab[read_times].tv_sec, times_tab[read_times].tv_nsec / 1000);
    } else if(read_times == TIMES_TAB_LEN) {
        snprintf(buff, BUFF_LEN, "Top-half executions: %d\n", irq_handler_count);
    } else if(read_times == TIMES_TAB_LEN + 1) {
        snprintf(buff, BUFF_LEN, "Bottom-half executions: %d\n", tasklet_count);
    } else {
        read_times = 0;
        return 0;
    }

    read_times++;

    len = strlen(buff);
    copy_to_user(user_buf, buff, len);
    return len;
}

static int error(const char* info, int errno)
{
    printk(KERN_ALERT "%s Error: %d\n", info, errno);
    disk_module_exit();
    return errno;
}
