#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");

static int disk_module_init(void);
static void disk_module_exit(void);
static void disk_tasklet(unsigned long times_tab_ptr);
irqreturn_t irq_disk_handler(int irq, void * data);
static int error(const char* info, int errno);

int disk_open(struct inode *inode, struct file *filp);
int disk_release(struct inode *inode, struct file *filp);
ssize_t disk_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos);;

struct file_operations disk_fops = {
    read: disk_read,
    open: disk_open,
    release: disk_release
};

module_init(disk_module_init);
module_exit(disk_module_exit);
