#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");

static int keyboard_module_init(void);
static void keyboard_module_exit(void);
static void keyboard_tasklet(unsigned long times_tab_ptr);
irqreturn_t irq_keyboard_handler(int irq, void * data);
static int error(const char* info, int errno);

int keyboard_open(struct inode *inode, struct file *filp);
int keyboard_release(struct inode *inode, struct file *filp);
ssize_t keyboard_read(struct file *filp, char *user_buf, size_t count, loff_t *f_pos);;

struct file_operations keyboard_fops = {
    read: keyboard_read,
    open: keyboard_open,
    release: keyboard_release
};

module_init(keyboard_module_init);
module_exit(keyboard_module_exit);
