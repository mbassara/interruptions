#!/bin/bash

cd ./src/modules/eth
sudo rmmod eth

cd ../kbrd
sudo rmmod kbrd

cd ../mouse
sudo rmmod mouse

cd ../disk
sudo rmmod disk
